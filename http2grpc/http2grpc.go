package http2grpc

import (
	"context"
	"github.com/golang/glog"
	"gitlab.com/zcodehelper/esrpcgateway"
	"gitlab.com/zcodehelper/esrpcgateway/esrpc"
	"gitlab.com/zcodehelper/zcodeutil"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"io"
	"net/http"
	"strconv"
	"strings"
	"sync/atomic"
	"time"
)

var http2grpcDebug = false

func Debug(on bool) {
	http2grpcDebug = on
}

//go:generate syncmap -name ThttpGatewayItemMap -pkg http2grpc map[string]*ThttpGatewayItem

// HttpPattern(sys, urlPathPrefix) -> *ThttpGatewayItem
var AllHttpGatewayItems = ThttpGatewayItemMap{}

type ThttpGatewayItem struct {
	sys                 string
	urlPathPrefix       string
	cors                string
	grpcServiceDiscover esrpcgateway.FnGrpcServiceDiscover
	httpPattern         string
	httpHandler         func(http.ResponseWriter, *http.Request)
}

func (this *ThttpGatewayItem) onHttpHandler(w http.ResponseWriter, r *http.Request) {
	if len(this.cors) > 0 {
		w.Header().Set("Access-Control-Allow-Origin", this.cors)
	}
	reqBin, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	rpcPkt := esrpc.RpcPacketFromVTPool()
	defer rpcPkt.ReturnToVTPool()

	err = rpcPkt.UnmarshalVT(reqBin)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if http2grpcDebug {
		glog.Info("http2grpc:onHttpHandler:", r.URL.String(), " method:", r.Method, " rpccode:", rpcPkt.RpcCode, " callid:", rpcPkt.RpcCallID())
	}

	switch rpcPkt.RpcCode {
	case esrpc.RpcCode_RPC_CODE_UNARY:
		this.onUnary(w, r, rpcPkt)
	case esrpc.RpcCode_RPC_CODE_CLIENT_STREAM, esrpc.RpcCode_RPC_CODE_SERVER_STREAM, esrpc.RpcCode_RPC_CODE_BIDI_STREAM:
		this.onRpcStream(w, r, rpcPkt)
	case esrpc.RpcCode_RPC_CODE_STREAM_END:
		//recv all server packet
		this.onStreamEnd(w, r, rpcPkt)
	case esrpc.RpcCode_RPC_CODE_STREAM_PACKET:
		this.onStreamPacket(w, r, rpcPkt)
	case esrpc.RpcCode_RPC_CODE_CLIENT_STREAM_CLOSE_SEND:
		this.onStreamCloseSend(w, r, rpcPkt)
	case esrpc.RpcCode_RPC_CODE_ABORT:
		this.onStreamAbort(w, r, rpcPkt)
	case esrpc.RpcCode_RPC_CODE_PING, esrpc.RpcCode_RPC_CODE_PONG:
		this.onStreamPing(w, r, rpcPkt)
	default:
		http.Error(w, "unknown rpc code:"+rpcPkt.RpcCode.String(), http.StatusBadRequest)
	}
	return
}

func (this *ThttpGatewayItem) onUnary(w http.ResponseWriter, r *http.Request, pkt *esrpc.RpcPacket) {
	if pkt.RpcOption == nil {
		//no option,can not process
		http.Error(w, "no rpc option", http.StatusBadRequest)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, "no rpc option", msg)
		return
	}
	sys := pkt.RpcOption["rpc-sys"]
	if this.sys != sys {
		http.Error(w, "sys not match:["+sys+"]:["+this.sys+"]", http.StatusBadRequest)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, "sys not match:["+sys+"]:["+this.sys+"]", msg)
		return
	}
	api := pkt.RpcOption["rpc-api"]
	grpcConn, err := this.grpcServiceDiscover(sys, api)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
		return
	}
	md, err := esrpcgateway.MetaDecode(pkt.PacketMetaHeader)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
		return
	}

	//fill ip
	if len(r.RemoteAddr) > 0 {
		md["ip"] = append(md["ip"], r.RemoteAddr)
	}

	ctx := metadata.NewOutgoingContext(context.Background(), md)

	var replyB []byte

	var header, trailer metadata.MD

	//add timeout
	rpcTimeout := esrpcgateway.DefaultCallTimeout
	rpcTimeoutMs, err := strconv.Atoi(pkt.RpcOption["rpc-timeout"])
	if err == nil {
		if rpcTimeoutMs < 1000 {
			rpcTimeoutMs = 1000
		}
		rpcTimeout = time.Duration(rpcTimeoutMs) * time.Millisecond
	}
	ctx, cancel := context.WithTimeout(ctx, rpcTimeout)
	defer cancel()

	donech := make(chan struct{})
	defer close(donech)

	go func() {
		tmr := time.NewTimer(rpcTimeout)
		defer tmr.Stop()
		select {
		case <-donech:
			return
		case <-tmr.C:
			cancel()
			return
		}
	}()

	replyB, err = grpcConn.InvokeForward(ctx, api, pkt.PacketBody,
		grpc.Header(&header),   // will retrieve header
		grpc.Trailer(&trailer), // will retrieve trailer
	)
	if http2grpcDebug {
		glog.Info(zcodeutil.NowTimeStrInLocal(), "UnaryCall:", api, "err:", err)
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
		return
	} else {
		headerData, _ := esrpcgateway.MetaEncode(header)
		trailerData, _ := esrpcgateway.MetaEncode(trailer)
		resRpcPkt := esrpc.RpcPacketFromVTPool()
		resRpcPkt.RpcCode = esrpc.RpcCode_RPC_CODE_UNARY_OK
		resRpcPkt.PacketBody = replyB
		resRpcPkt.PacketMetaHeader = headerData
		resRpcPkt.PacketMetaTrailer = trailerData
		resRpcPkt.SeqNo = pkt.SeqNo
		defer resRpcPkt.ReturnToVTPool()
		data, err := resRpcPkt.MarshalVT()
		if err != nil {
			glog.Info("onUnary MarshalVT err:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
			return

		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(data)
		return
	}
}

func (this *ThttpGatewayItem) onRpcStream(w http.ResponseWriter, r *http.Request, pkt *esrpc.RpcPacket) {
	if pkt.RpcOption == nil {
		//no option,can not process
		http.Error(w, "no rpc option", http.StatusBadRequest)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, "no rpc option", msg)
		return
	}
	sys := pkt.RpcOption["rpc-sys"]
	if this.sys != sys {
		http.Error(w, "sys not match:["+sys+"]:["+this.sys+"]", http.StatusBadRequest)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, "sys not match:["+sys+"]:["+this.sys+"]", msg)
		return
	}
	callid := pkt.RpcOption["rpc-callid"]
	if len(callid) == 0 {
		http.Error(w, "no rpc-callid in  rpc option", http.StatusBadRequest)
		return
	}
	api := pkt.RpcOption["rpc-api"]
	if len(api) == 0 {
		http.Error(w, "no rpc-api in  rpc option", http.StatusBadRequest)
		return
	}
	grpcConn, err := this.grpcServiceDiscover(sys, api)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
		return
	}

	md, err := esrpcgateway.MetaDecode(pkt.PacketMetaHeader)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		//this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
		return
	}

	rpcStream := &ThttpGrpcStream{
		rpcType:      pkt.RpcCode,
		api:          api,
		sys:          sys,
		callid:       callid,
		grpcStream:   nil,
		GrpcConn:     grpcConn,
		CancelFn:     nil,
		HasCancel:    atomic.Bool{},
		destroyed:    atomic.Bool{},
		lastGrpcTime: time.Now(),
		lastHttpTime: time.Now(),
	}

	err = rpcStream.setup(md)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		rpcStream.destroy()
		return
	}
	w.WriteHeader(http.StatusOK)
	return

}

func (this *ThttpGatewayItem) onStreamPacket(w http.ResponseWriter, r *http.Request, pkt *esrpc.RpcPacket) {
	callid := pkt.RpcCallID()
	if len(callid) == 0 {
		http.Error(w, "no rpc-callid in  rpc option", http.StatusBadRequest)
		return
	}
	rpcStream, ok := AllHttpGrpcStreams.Load(callid)
	if !ok {
		http.Error(w, "no stream found", http.StatusBadRequest)
		return
	}

	rpcStream.onStreamPacket(w, r, pkt)
	return
}

func (this *ThttpGatewayItem) onStreamCloseSend(w http.ResponseWriter, r *http.Request, pkt *esrpc.RpcPacket) {
	callid := pkt.RpcCallID()
	if len(callid) == 0 {
		http.Error(w, "no rpc-callid in  rpc option", http.StatusBadRequest)
		return
	}

	rpcStream, ok := AllHttpGrpcStreams.Load(callid)
	if !ok {
		http.Error(w, "no stream found", http.StatusBadRequest)
		return
	}

	rpcStream.onStreamCloseSend(w, r, pkt)
	return
}

func (this *ThttpGatewayItem) onStreamAbort(w http.ResponseWriter, r *http.Request, pkt *esrpc.RpcPacket) {
	callid := pkt.RpcCallID()
	if len(callid) == 0 {
		http.Error(w, "no rpc-callid in  rpc option", http.StatusBadRequest)
		return
	}
	rpcStream, ok := AllHttpGrpcStreams.Load(callid)
	if !ok {
		http.Error(w, "no stream found", http.StatusNotFound)
		return
	}
	rpcStream.destroy()

	w.WriteHeader(http.StatusOK)
	return
}

func (this *ThttpGatewayItem) onStreamPing(w http.ResponseWriter, r *http.Request, pkt *esrpc.RpcPacket) {
	callid := pkt.RpcCallID()
	if len(callid) == 0 {
		http.Error(w, "no rpc-callid in  rpc option", http.StatusBadRequest)
		return
	}

	rpcStream, ok := AllHttpGrpcStreams.Load(callid)
	if !ok {
		http.Error(w, "no stream found", http.StatusNotFound)
		return
	}

	rpcStream.lastHttpTime = time.Now()

	w.WriteHeader(http.StatusOK)
	return
}

func (this *ThttpGatewayItem) onStreamEnd(w http.ResponseWriter, r *http.Request, pkt *esrpc.RpcPacket) {
	callid := pkt.RpcCallID()
	if len(callid) == 0 {
		http.Error(w, "no rpc-callid in  rpc option", http.StatusBadRequest)
		return
	}

	rpcStream, ok := AllHttpGrpcStreams.Load(callid)
	if !ok {
		http.Error(w, "no stream found", http.StatusBadRequest)
		return
	}

	rpcStream.onClientGetStreamResult(w, r, pkt)
	return
}

func HttpPattern(sys, urlPathPrefix string) string {
	r := urlPathPrefix
	if !strings.HasSuffix(r, "/") {
		r += "/"
	}
	r += sys
	if !strings.HasSuffix(r, "/") {
		r += "/"
	}
	r += "grpc/"
	return r

}

// cors: if set, write http header: Access-Control-Allow-Origin=cors
func SetupGrpcGateWay(sys string, urlPrefix string, cors string, grpcServiceDiscover esrpcgateway.FnGrpcServiceDiscover) (httpPattern string, httpHandler func(http.ResponseWriter, *http.Request), gateway *ThttpGatewayItem, err error) {
	httpPattern = HttpPattern(sys, urlPrefix)
	if gateway, ok := AllHttpGatewayItems.Load(httpPattern); ok {
		return httpPattern, gateway.httpHandler, gateway, nil
	}
	gateway = &ThttpGatewayItem{
		sys:                 sys,
		urlPathPrefix:       urlPrefix,
		cors:                cors,
		grpcServiceDiscover: grpcServiceDiscover,
	}

	gateway.httpHandler = func(w http.ResponseWriter, r *http.Request) {
		gateway.onHttpHandler(w, r)
	}

	AllHttpGatewayItems.Store(httpPattern, gateway)
	go checkDeadStreamRoutine()
	return httpPattern, gateway.httpHandler, gateway, nil
}

var checkDeadStreamRoutineStarted = atomic.Bool{}

func checkDeadStreamRoutine() {
	if !checkDeadStreamRoutineStarted.CompareAndSwap(false, true) {
		return
	}
	for {
		time.Sleep(time.Minute)
		AllHttpGrpcStreams.Range(func(callid string, rpcstream *ThttpGrpcStream) bool {
			if rpcstream.streamIsDead() {
				glog.Info("checkDeadStreamRoutine:stream is dead, callid:", callid, rpcstream.api)
				go rpcstream.destroy()
			}
			return true
		})
	}
}

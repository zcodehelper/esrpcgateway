package http2grpc

import (
	"bytes"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/golang/glog"
	"gitlab.com/zcodehelper/esrpcgateway"
	"gitlab.com/zcodehelper/esrpcgateway/esrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"io"
	"net/http"
	"strconv"
	"sync/atomic"
	"time"
)

var httpMagic = []byte{0x12, 0x34}

//go:generate syncmap -name ThttpGrpcStreamMap -pkg http2grpc map[string]*ThttpGrpcStream

// callid -> *ThttpGrpcStream
var AllHttpGrpcStreams = ThttpGrpcStreamMap{}

type ThttpGrpcStream struct {
	rpcType esrpc.RpcCode
	api     string
	sys     string
	callid  string

	grpcStream  grpc.ClientStream
	GrpcConn    *grpc.ClientConn
	CancelFn    context.CancelFunc
	HasCancel   atomic.Bool
	destroyed   atomic.Bool
	keepAliveCh chan struct{}

	lastGrpcTime      time.Time
	lastHttpTime      time.Time
	clientPacketCount uint32
	serverPacketCount uint32
	seqno             uint32
}

func (this *ThttpGrpcStream) keepStreamAlive(w http.ResponseWriter) {
	this.keepAliveCh = make(chan struct{})
	ticker := time.NewTicker(time.Minute)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			if this.destroyed.Load() || this.HasCancel.Load() {
				return
			}
			if time.Since(this.lastGrpcTime) >= time.Minute {
				resPacket := &esrpc.RpcPacket{
					RpcCode: esrpc.RpcCode_RPC_CODE_PING,
				}
				_ = this.sendClientRpcPacket(resPacket, w)
			}
		case <-this.keepAliveCh:
			return
		}
	}

}
func (this *ThttpGrpcStream) onClientGetStreamResult(w http.ResponseWriter, r *http.Request, rpcPkt *esrpc.RpcPacket) {
	defer this.destroy()
	header, err := this.grpcStream.Header()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	go this.keepStreamAlive(w)

	recvMsgCount := 0
	for {
		if this.HasCancel.Load() || this.destroyed.Load() {
			break
		}
		responseBytes, err := this.grpcStream.RecvMsgForward()
		if this.HasCancel.Load() || this.destroyed.Load() {
			break
		}

		if err != nil && recvMsgCount == 0 {
			if status.Code(err) == codes.Internal {
				//if server stream send nothing and end the stream, client may recv Internal error
				fmt.Println("recvGrpc err with recvcount=0,treat it as eof:", this.api, err)
				err = io.EOF
			}
		}

		if err == io.EOF {
			err = this.RPCEnd(w)
			break
		}
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			break
		}
		err = this.sendClientResponse(responseBytes, header, w)
		if err != nil {
			break
		}
		header = nil
		this.lastGrpcTime = time.Now()
		recvMsgCount++
	}
	if err != nil && http2grpcDebug {
		glog.Errorf("api: %s http on client get stream result error:%s", this.api, err)
		//http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
func (this *ThttpGrpcStream) getSeqNo() uint32 {
	this.seqno++
	return this.seqno
}
func (this *ThttpGrpcStream) sendClientRpcPacket(pkt *esrpc.RpcPacket, w http.ResponseWriter) error {
	data, err := pkt.MarshalVT()
	if err != nil {
		return err
	}
	this.lastHttpTime = time.Now()

	buf := bytes.Buffer{}
	buf.Grow(len(data) + 4 + 2)

	_, err = buf.Write(httpMagic)
	if err != nil {
		return err
	}

	data_len_buf := make([]byte, 4)
	binary.LittleEndian.PutUint32(data_len_buf, uint32(len(data)))
	_, err = buf.Write(data_len_buf)
	if err != nil {
		return err
	}

	_, err = buf.Write(data)
	if err != nil {
		return err
	}

	_, err = w.Write(buf.Bytes())
	if err != nil {
		return err
	}
	if f, ok := w.(http.Flusher); ok {
		f.Flush()
	}
	if http2grpcDebug {
		glog.Infof("send client rpc packet api:%s data len:%d %s", this.api, len(data), pkt)
	}

	return nil

}
func (this *ThttpGrpcStream) sendClientResponse(b []byte, header metadata.MD, w http.ResponseWriter) error {
	if this.destroyed.Load() || this.HasCancel.Load() {
		return nil
	}
	headerBytes, _ := esrpcgateway.MetaEncode(header)
	resRpcPkt := esrpc.RpcPacketFromVTPool()
	resRpcPkt.RpcCode = esrpc.RpcCode_RPC_CODE_STREAM_PACKET
	resRpcPkt.PacketBody = b
	resRpcPkt.PacketMetaHeader = headerBytes
	resRpcPkt.SeqNo = this.getSeqNo()
	resRpcPkt.RpcOption = map[string]string{"rpc-server-pkt-no": strconv.FormatInt(int64(this.serverPacketCount), 10)}
	defer resRpcPkt.ReturnToVTPool()
	this.serverPacketCount++

	return this.sendClientRpcPacket(resRpcPkt, w)
}

func (this *ThttpGrpcStream) onStreamCloseSend(w http.ResponseWriter, r *http.Request, rpcPkt *esrpc.RpcPacket) {
	clientPktCount := rpcPkt.GetClientPktCount()
	if this.clientPacketCount != clientPktCount {
		err := errors.New("client packet count not match got:" + strconv.FormatInt(int64(this.clientPacketCount), 10) + " send:" +
			strconv.FormatInt(int64(clientPktCount), 10))
		http.Error(w, err.Error(), http.StatusBadRequest)
		this.destroy()
		return
	}
	err := this.grpcStream.CloseSend()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		this.destroy()
		return
	}
	w.WriteHeader(http.StatusOK)
	resRpcPkt := &esrpc.RpcPacket{
		RpcCode: esrpc.RpcCode_RPC_CODE_CLIENT_STREAM_CLOSE_SEND_OK,
		SeqNo:   this.getSeqNo(),
	}
	dAtA, err := resRpcPkt.MarshalVT()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		this.destroy()
		return
	}
	_, err = w.Write(dAtA)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		this.destroy()
		return
	}
	return
}
func (this *ThttpGrpcStream) onStreamPacket(w http.ResponseWriter, r *http.Request, rpcPkt *esrpc.RpcPacket) {
	this.clientPacketCount++
	err := this.grpcStream.SendMsgForward(rpcPkt.PacketBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		this.destroy()
		return
	}

	if this.rpcType == esrpc.RpcCode_RPC_CODE_SERVER_STREAM {
		err = this.grpcStream.CloseSend()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			this.destroy()
			return
		}
	}

	w.WriteHeader(http.StatusOK)
	return

}
func (this *ThttpGrpcStream) StreamDesc() *grpc.StreamDesc {
	return &grpc.StreamDesc{
		ClientStreams: this.rpcType == esrpc.RpcCode_RPC_CODE_CLIENT_STREAM || this.rpcType == esrpc.RpcCode_RPC_CODE_BIDI_STREAM,
		ServerStreams: this.rpcType == esrpc.RpcCode_RPC_CODE_SERVER_STREAM || this.rpcType == esrpc.RpcCode_RPC_CODE_BIDI_STREAM,
	}
}
func (this *ThttpGrpcStream) setup(md metadata.MD) (err error) {
	ctx, cancelFn := context.WithCancel(metadata.NewOutgoingContext(context.Background(), md))
	this.grpcStream, err = grpc.NewClientStream(ctx, this.StreamDesc(), this.GrpcConn, this.api)
	if err != nil {
		fmt.Println("make rpc stream err:", err, this.api)
		cancelFn()
		return err
	}
	this.CancelFn = cancelFn

	AllHttpGrpcStreams.Store(this.callid, this)

	return nil
}

func (this *ThttpGrpcStream) destroy() {
	if !this.destroyed.CompareAndSwap(false, true) {
		return
	}
	if http2grpcDebug {
		glog.Info("destroy http->grpc stream:", this.sys, this.api, this.callid)
	}
	AllHttpGrpcStreams.Delete(this.callid)
	this.HasCancel.Store(true)
	cancelfn := this.CancelFn
	this.CancelFn = nil
	if cancelfn != nil {
		cancelfn()
	}
	if this.keepAliveCh != nil {
		close(this.keepAliveCh)
	}
}

func (this *ThttpGrpcStream) RPCEnd(w http.ResponseWriter) error {
	tailer := this.grpcStream.Trailer()

	tailerBytes, _ := esrpcgateway.MetaEncode(tailer)

	resRpcPkt := esrpc.RpcPacketFromVTPool()
	resRpcPkt.RpcCode = esrpc.RpcCode_RPC_CODE_STREAM_END
	resRpcPkt.PacketMetaTrailer = tailerBytes
	resRpcPkt.SeqNo = this.getSeqNo()
	resRpcPkt.RpcOption = map[string]string{"rpc-server-pkt-count": strconv.FormatInt(int64(this.serverPacketCount), 10)}
	defer resRpcPkt.ReturnToVTPool()

	return this.sendClientRpcPacket(resRpcPkt, w)

}
func (this *ThttpGrpcStream) streamIsDead() bool {
	if time.Since(this.lastHttpTime) > time.Minute*3 && time.Since(this.lastGrpcTime) > time.Minute*3 {
		return true
	}
	return false
}

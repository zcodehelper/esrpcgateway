package esrpcgateway

import (
	"google.golang.org/grpc"
	"time"
)

type FnGrpcServiceDiscover func(sys string, api string) (grpcConn *grpc.ClientConn, err error)

var DefaultCallTimeout = 10 * time.Second

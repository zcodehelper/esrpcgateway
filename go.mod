module gitlab.com/zcodehelper/esrpcgateway

go 1.19

require (
	github.com/golang/glog v1.0.0
	github.com/nats-io/nats.go v1.22.1
	gitlab.com/zcodehelper/zcodeutil v0.0.17
	google.golang.org/grpc v1.51.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/nats-io/nats-server/v2 v2.8.4 // indirect
	github.com/nats-io/nkeys v0.3.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/uuid6/uuid6go-proto v0.2.1 // indirect
	golang.org/x/crypto v0.0.0-20220315160706-3147a52a75dd // indirect
	golang.org/x/net v0.2.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/genproto v0.0.0-20221118155620-16455021b5e6 // indirect
)

replace google.golang.org/grpc => github.com/yangjuncode/grpc-go v1.51.1

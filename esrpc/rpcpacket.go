package esrpc

import "strconv"

func (this *RpcPacket) RpcCallID() string {
	if this.RpcOption == nil {
		return ""
	}
	return this.RpcOption["rpc-callid"]
}

func (this *RpcPacket) GetClientPktCount() uint32 {
	if this.RpcOption == nil {
		return 0
	}
	countStr := this.RpcOption["rpc-client-pkt-count"]
	if len(countStr) == 0 {
		return 0
	}
	count, _ := strconv.ParseUint(countStr, 10, 32)
	return uint32(count)
}

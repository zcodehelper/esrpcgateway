package esrpcgateway

import (
	"bufio"
	"bytes"
	"errors"
	"google.golang.org/grpc/metadata"
	"io"
	"net/http"
	"net/textproto"
	"strings"
)

var ErrBadHeaderMsg = errors.New("MetaDecode: could not decode headers")

func MetaEncode(md metadata.MD) (data []byte, err error) {
	var b bytes.Buffer

	err = http.Header(md).Write(&b)
	if err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}

func MetaDecode(data []byte) (md metadata.MD, err error) {
	md = make(metadata.MD)
	if len(data) == 0 {
		return
	}
	tp := textproto.NewReader(bufio.NewReader(bytes.NewReader(data)))
	for {
		kv, err := tp.ReadLine()
		if len(kv) == 0 {
			if err == io.EOF {
				err = nil
			}
			return md, err
		}

		// Process key fetching original case.
		i := bytes.IndexByte([]byte(kv), ':')
		if i < 0 {
			return nil, ErrBadHeaderMsg
		}
		key := kv[:i]
		if key == "" {
			// Skip empty keys.
			continue
		}
		key = strings.ToLower(key)
		i++
		for i < len(kv) && (kv[i] == ' ' || kv[i] == '\t') {
			i++
		}
		value := string(kv[i:])
		md[key] = append(md[key], value)
		if err != nil {
			return md, err
		}
	}
}

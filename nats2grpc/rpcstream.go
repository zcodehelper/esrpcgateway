package nats2grpc

import (
	"context"
	"errors"
	"fmt"
	"github.com/golang/glog"
	"github.com/nats-io/nats.go"
	"gitlab.com/zcodehelper/esrpcgateway"
	"gitlab.com/zcodehelper/esrpcgateway/esrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"io"
	"strconv"
	"sync/atomic"
	"time"
)

type TgrpcStream struct {
	rpcType      esrpc.RpcCode
	api          string
	sys          string
	callid       string
	natsclientid string

	natsConn        *nats.Conn
	apiSubscription atomic.Pointer[nats.Subscription]

	grpcStream grpc.ClientStream
	GrpcConn   *grpc.ClientConn
	CancelFn   context.CancelFunc
	HasCancel  atomic.Bool
	destroyed  atomic.Bool

	lastGrpcTime      time.Time
	lastNatsTime      time.Time
	clientPacketCount uint32
	serverPacketCount uint32
	seqno             uint32
}

func (this *TgrpcStream) getSeqNo() uint32 {
	this.seqno++
	return this.seqno
}

func (this *TgrpcStream) destroy() {
	if !this.destroyed.CompareAndSwap(false, true) {
		return
	}
	if nats2grpcDebug {
		glog.Info("destroy grpc stream:", this.sys, this.api, this.callid)
	}
	this.HasCancel.Store(true)
	subscription := this.apiSubscription.Load()
	this.apiSubscription.Store(nil)
	if subscription != nil {
		_ = subscription.Unsubscribe()
	}
	cancelfn := this.CancelFn
	this.CancelFn = nil
	if cancelfn != nil {
		cancelfn()
	}
}

func (this *TgrpcStream) clientSendSubject() string {
	if len(this.sys) == 0 {
		return "grpcstream." + this.callid
	} else {
		return this.sys + ".grpcstream." + this.callid
	}
}
func (this *TgrpcStream) clientRecvSubject(msg *nats.Msg) string {
	s := this.natsclientid
	if msg != nil && len(msg.Reply) > 0 {
		s = msg.Reply
	}
	return s
}

func (this *TgrpcStream) StreamDesc() *grpc.StreamDesc {
	return &grpc.StreamDesc{
		ClientStreams: this.rpcType == esrpc.RpcCode_RPC_CODE_CLIENT_STREAM || this.rpcType == esrpc.RpcCode_RPC_CODE_BIDI_STREAM,
		ServerStreams: this.rpcType == esrpc.RpcCode_RPC_CODE_SERVER_STREAM || this.rpcType == esrpc.RpcCode_RPC_CODE_BIDI_STREAM,
	}
}

func (this *TgrpcStream) setup(md metadata.MD) (err error) {
	apiSubscription, err := this.natsConn.SubscribeSync(this.clientSendSubject())
	if err != nil {
		return err
	}

	this.apiSubscription.Store(apiSubscription)

	ctx, cancelFn := context.WithCancel(metadata.NewOutgoingContext(context.Background(), md))
	this.grpcStream, err = grpc.NewClientStream(ctx, this.StreamDesc(), this.GrpcConn, this.api)
	if err != nil {
		fmt.Println("make rpc stream err:", err, this.api)
		cancelFn()
		return err
	}
	this.CancelFn = cancelFn

	return nil
}

func (this *TgrpcStream) publishErr(pkt *esrpc.RpcPacket, rpcCode esrpc.RpcCode, err error, msg *nats.Msg) {
	var seqno uint32 = 0
	if pkt != nil {
		seqno = pkt.SeqNo
	} else {
		seqno = this.getSeqNo()
	}
	resRpcPkt := esrpc.RpcPacketFromVTPool()
	resRpcPkt.RpcCode = rpcCode
	resRpcPkt.RpcOption = map[string]string{"rpc-err": err.Error()}
	resRpcPkt.SeqNo = seqno
	defer resRpcPkt.ReturnToVTPool()

	subject := this.clientRecvSubject(msg)
	data, _ := resRpcPkt.MarshalVT()
	natsHeader := map[string][]string{"rpc-callid": {this.callid}}
	resMsg := &nats.Msg{
		Subject: subject,
		Reply:   "",
		Header:  natsHeader,
		Data:    data,
		Sub:     nil,
	}
	_ = this.PublishMsg(resMsg)
}
func (this *TgrpcStream) publishOK(pkt *esrpc.RpcPacket, okCode esrpc.RpcCode, msg *nats.Msg) {
	var seqno uint32 = 0
	if pkt != nil {
		seqno = pkt.SeqNo
	} else {
		seqno = this.getSeqNo()
	}

	resRpcPkt := esrpc.RpcPacketFromVTPool()
	resRpcPkt.RpcCode = okCode
	resRpcPkt.SeqNo = seqno
	defer resRpcPkt.ReturnToVTPool()

	subject := this.clientRecvSubject(msg)
	data, _ := resRpcPkt.MarshalVT()
	natsHeader := map[string][]string{"rpc-callid": {this.callid}}
	resMsg := &nats.Msg{
		Subject: subject,
		Reply:   "",
		Header:  natsHeader,
		Data:    data,
		Sub:     nil,
	}
	_ = this.PublishMsg(resMsg)
}
func (this *TgrpcStream) onNatsMsg(msg *nats.Msg) (err error) {
	this.lastNatsTime = time.Now()

	rpcPkt := &esrpc.RpcPacket{}

	err = rpcPkt.UnmarshalVT(msg.Data)
	if err != nil {
		glog.Info("TgrpcStream.onNatsMsg UnmarshalVT err:", err, msg.Subject)
		return err
	}
	if nats2grpcDebug {
		glog.Info("TgrpcStream.onNatsMsg:", rpcPkt)
	}
	switch rpcPkt.RpcCode {
	case esrpc.RpcCode_RPC_CODE_STREAM_PACKET:
		this.clientPacketCount++
		err = this.grpcStream.SendMsgForward(rpcPkt.PacketBody)
		if err != nil {
			this.publishErr(rpcPkt, esrpc.RpcCode_RPC_CODE_STREAM_PACKET_ERR, err, msg)
			this.destroy()
			return err
		}
		if len(msg.Reply) > 0 {
			//response send ok to client
			this.publishOK(rpcPkt, esrpc.RpcCode_RPC_CODE_STREAM_PACKET_OK, msg)
		}
		if this.rpcType == esrpc.RpcCode_RPC_CODE_SERVER_STREAM {
			err = this.grpcStream.CloseSend()
			if err != nil {
				this.publishErr(rpcPkt, esrpc.RpcCode_RPC_CODE_ERR, err, msg)
				this.destroy()
				return err
			}
		}
	case esrpc.RpcCode_RPC_CODE_CLIENT_STREAM_CLOSE_SEND:
		clientPktCount := rpcPkt.GetClientPktCount()
		if this.clientPacketCount != clientPktCount {
			err = errors.New("client packet count not match got:" + strconv.FormatInt(int64(this.clientPacketCount), 10) + " send:" +
				strconv.FormatInt(int64(clientPktCount), 10))
			this.publishErr(rpcPkt, esrpc.RpcCode_RPC_CODE_CLIENT_STREAM_CLOSE_SEND_ERR, err, msg)
			this.destroy()
			return errors.New("client packet count not match")
		}
		err = this.grpcStream.CloseSend()
		if err != nil {
			this.publishErr(rpcPkt, esrpc.RpcCode_RPC_CODE_CLIENT_STREAM_CLOSE_SEND_ERR, err, msg)
			return err
		}
		this.publishOK(rpcPkt, esrpc.RpcCode_RPC_CODE_CLIENT_STREAM_CLOSE_SEND_OK, msg)
	case esrpc.RpcCode_RPC_CODE_PING:
		this.onPing(rpcPkt, msg)
	case esrpc.RpcCode_RPC_CODE_PONG:
		this.onPong(rpcPkt, msg)
	case esrpc.RpcCode_RPC_CODE_ABORT:
		this.onAbort(rpcPkt, msg)
	default:
		if nats2grpcDebug {
			glog.Info("TgrpcStream) onNatsMsg unprocessing rpcCode:", esrpc.RpcCode_name[int32(rpcPkt.RpcCode)], msg.Subject)
		}
	}

	return err

}

func (this *TgrpcStream) run() {

	defer this.destroy()

	go this.recvGrpc()

	timeoutCount := 0
	for {
		//recvGrpc client msg
		apiSubscription := this.apiSubscription.Load()
		if apiSubscription == nil {
			break
		}
		msg, err := apiSubscription.NextMsg(time.Second * 10)
		if this.destroyed.Load() || this.HasCancel.Load() {
			return
		}
		if err != nil {
			if err == nats.ErrTimeout {
				timeoutCount++
				if timeoutCount > 3 {
					this.ping()
				}
				if this.streamIsDead() {
					return
				}
				continue
			}
			this.publishErr(nil, esrpc.RpcCode_RPC_CODE_ERR, err, nil)
			return
		}
		timeoutCount = 0
		err = this.onNatsMsg(msg)
		if err != nil {
			return
		}
		this.lastNatsTime = time.Now()
	}

}

func (this *TgrpcStream) recvGrpc() {
	defer this.destroy()
	header, err := this.grpcStream.Header()
	if err != nil {
		this.publishErr(nil, esrpc.RpcCode_RPC_CODE_ERR, err, nil)
		return
	}

	recvMsgCount := 0

	for {

		responseBytes, err := this.grpcStream.RecvMsgForward()
		if this.HasCancel.Load() || this.destroyed.Load() {
			break
		}

		if err != nil && recvMsgCount == 0 {
			if status.Code(err) == codes.Internal {
				//if server stream send nothing and end the stream, client may recv Internal error
				fmt.Println("recvGrpc err with recvcount=0,treat it as eof:", this.api, err)
				err = io.EOF
			}
		}

		if err == io.EOF {
			this.RPCEnd()
			break
		}
		if err != nil {
			this.publishErr(nil, esrpc.RpcCode_RPC_CODE_ERR, err, nil)
			break
		}
		this.sendClientResponse(responseBytes, header)
		header = nil
		this.lastGrpcTime = time.Now()
		recvMsgCount++
	}

}

func (this *TgrpcStream) ping() {
	if nats2grpcDebug {
		glog.Info("TgrpcStream.ping api:", this.api, " natsid:", this.natsclientid)
	}
	this.publishOK(nil, esrpc.RpcCode_RPC_CODE_PING, nil)
}

func (this *TgrpcStream) onPing(pkt *esrpc.RpcPacket, msg *nats.Msg) {
	this.publishOK(pkt, esrpc.RpcCode_RPC_CODE_PONG, msg)
}

func (this *TgrpcStream) onPong(rpcPkg *esrpc.RpcPacket, msg *nats.Msg) {
	//no need to do anything

}
func (this *TgrpcStream) sendAbortOK(pkt *esrpc.RpcPacket, msg *nats.Msg) {
	this.publishOK(pkt, esrpc.RpcCode_RPC_CODE_ABORT_OK, msg)
}
func (this *TgrpcStream) onAbort(pkt *esrpc.RpcPacket, msg *nats.Msg) {
	this.destroy()
	this.sendAbortOK(pkt, msg)
}
func (this *TgrpcStream) PublishMsg(msg *nats.Msg) error {
	if nats2grpcDebug {
		resPack := &esrpc.RpcPacket{}
		_ = resPack.UnmarshalVT(msg.Data)
		glog.Info("TgrpcStream.publishMsg msg:", " sys:", this.sys, " api:", this.api, " subject:", msg.Subject, " rpcPkt:", resPack)

	}
	return this.natsConn.PublishMsg(msg)
}
func (this *TgrpcStream) RPCEnd() {
	tailer := this.grpcStream.Trailer()

	tailerBytes, _ := esrpcgateway.MetaEncode(tailer)

	resRpcPkt := esrpc.RpcPacketFromVTPool()
	resRpcPkt.RpcCode = esrpc.RpcCode_RPC_CODE_STREAM_END
	resRpcPkt.PacketMetaTrailer = tailerBytes
	resRpcPkt.SeqNo = this.getSeqNo()
	resRpcPkt.RpcOption = map[string]string{"rpc-server-pkt-count": strconv.FormatInt(int64(this.serverPacketCount), 10)}
	defer resRpcPkt.ReturnToVTPool()

	subject := this.natsclientid
	data, _ := resRpcPkt.MarshalVT()
	natsHeader := map[string][]string{"rpc-callid": {this.callid}}
	resMsg := &nats.Msg{
		Subject: subject,
		Reply:   "",
		Header:  natsHeader,
		Data:    data,
		Sub:     nil,
	}
	err := this.PublishMsg(resMsg)
	if err != nil {
		this.publishErr(nil, esrpc.RpcCode_RPC_CODE_ERR, err, nil)
		this.destroy()
	}

}

func (this *TgrpcStream) sendClientResponse(b []byte, header metadata.MD) {
	if this.destroyed.Load() || this.HasCancel.Load() {
		return
	}
	headerBytes, _ := esrpcgateway.MetaEncode(header)

	resRpcPkt := esrpc.RpcPacketFromVTPool()
	resRpcPkt.RpcCode = esrpc.RpcCode_RPC_CODE_STREAM_PACKET
	resRpcPkt.PacketBody = b
	resRpcPkt.PacketMetaHeader = headerBytes
	resRpcPkt.SeqNo = this.getSeqNo()
	resRpcPkt.RpcOption = map[string]string{"rpc-server-pkt-no": strconv.FormatInt(int64(this.serverPacketCount), 10)}
	defer resRpcPkt.ReturnToVTPool()

	this.serverPacketCount++

	subject := this.natsclientid
	data, _ := resRpcPkt.MarshalVT()
	natsHeader := map[string][]string{"rpc-callid": {this.callid}}
	resMsg := &nats.Msg{
		Subject: subject,
		Reply:   "",
		Header:  natsHeader,
		Data:    data,
		Sub:     nil,
	}
	err := this.PublishMsg(resMsg)
	if err != nil {
		this.publishErr(nil, esrpc.RpcCode_RPC_CODE_ERR, err, nil)
		this.destroy()
	}
}

func (this *TgrpcStream) streamIsDead() bool {
	if time.Since(this.lastNatsTime) > time.Minute*3 && time.Since(this.lastGrpcTime) > time.Minute*3 {
		return true
	}
	return false
}

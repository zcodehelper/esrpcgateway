package nats2grpc

import (
	"context"
	"github.com/golang/glog"
	"github.com/nats-io/nats.go"
	"gitlab.com/zcodehelper/esrpcgateway"
	"gitlab.com/zcodehelper/esrpcgateway/esrpc"
	"gitlab.com/zcodehelper/zcodeutil"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"strconv"
	"sync/atomic"
	"time"
)

var nats2grpcDebug = false

// Debug turn on/off nats2grpcDebug output
func Debug(on bool) {
	nats2grpcDebug = on
}

//go:generate syncmap -name TgatewayItemMap -pkg nats2grpc map[string]*TgatewayItem

var nats2grpcGateways = TgatewayItemMap{}

type TgatewayItem struct {
	sys                 string
	natsConn            *nats.Conn
	apiSubscription     *nats.Subscription
	grpcServiceDiscover esrpcgateway.FnGrpcServiceDiscover
}

func (this *TgatewayItem) onNatsMsg(msg *nats.Msg) {

	rpcPkt := &esrpc.RpcPacket{}
	err := rpcPkt.UnmarshalVT(msg.Data)
	if nats2grpcDebug && err != nil {
		this.publishErr(rpcPkt, esrpc.RpcCode_RPC_CODE_ERR, err.Error(), msg)
		glog.Info("TgatewayItem.onNatsMsg err:", err, msg.Subject)
		return
	}
	if nats2grpcDebug {
		glog.Info("TgatewayItem.onNatsMsg:", msg.Subject, rpcPkt)
	}
	switch rpcPkt.RpcCode {
	case esrpc.RpcCode_RPC_CODE_UNARY:
		go this.onUnary(rpcPkt, msg)
	case esrpc.RpcCode_RPC_CODE_CLIENT_STREAM, esrpc.RpcCode_RPC_CODE_SERVER_STREAM, esrpc.RpcCode_RPC_CODE_BIDI_STREAM:
		go this.onRpcStream(rpcPkt, msg)
	default:
		if nats2grpcDebug {
			glog.Info("TgatewayItem.onNatsMsg bad rpcCode:", esrpc.RpcCode_name[int32(rpcPkt.RpcCode)], msg.Subject)
		}
	}

}
func (this *TgatewayItem) setupApiSubscription() (err error) {
	subject := this.sys
	queue := this.sys
	if len(subject) == 0 {
		subject = "grpc.>"
		queue = "nats-grpc"
	} else {
		subject += ".grpc.>"
		queue += "-nats-grpc"
	}
	this.apiSubscription, err = this.natsConn.QueueSubscribe(subject, queue, func(msg *nats.Msg) {
		this.onNatsMsg(msg)
	})
	if err != nil {
		glog.Info("setupApiSubscription err:", err)
	}
	return err
}
func (this *TgatewayItem) publishErr(pkt *esrpc.RpcPacket, code esrpc.RpcCode, err string, msg *nats.Msg) {
	var seqno uint32 = 0
	if pkt != nil {
		seqno = pkt.SeqNo
	}
	resRpcPkt := esrpc.RpcPacketFromVTPool()
	resRpcPkt.RpcCode = code
	resRpcPkt.RpcOption = map[string]string{"rpc-err": err}
	resRpcPkt.SeqNo = seqno
	defer resRpcPkt.ReturnToVTPool()
	data, _ := resRpcPkt.MarshalVT()
	_ = this.natsConn.Publish(msg.Reply, data)

}
func (this *TgatewayItem) publishConnectStreamOK(pkt *esrpc.RpcPacket, code esrpc.RpcCode, msg *nats.Msg) {
	var seqno uint32 = 0
	if pkt != nil {
		seqno = pkt.SeqNo
	}

	resRpcPkt := esrpc.RpcPacketFromVTPool()
	resRpcPkt.RpcCode = code
	resRpcPkt.SeqNo = seqno
	defer resRpcPkt.ReturnToVTPool()
	data, _ := resRpcPkt.MarshalVT()
	_ = this.natsConn.Publish(msg.Reply, data)

}

func (this *TgatewayItem) onUnary(pkt *esrpc.RpcPacket, msg *nats.Msg) {
	if len(msg.Reply) == 0 {
		//no reply,no need to reply
		return
	}
	if pkt.RpcOption == nil {
		//no option,can not process
		this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, "no rpc option", msg)
		return
	}
	sys := pkt.RpcOption["rpc-sys"]
	if this.sys != sys {
		this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, "sys not match:["+sys+"]:["+this.sys+"]", msg)
		return
	}
	api := pkt.RpcOption["rpc-api"]
	natsID := pkt.RpcOption["rpc-nats-id"]
	grpcConn, err := this.grpcServiceDiscover(sys, api)
	if err != nil {
		this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
		return
	}
	md, err := esrpcgateway.MetaDecode(pkt.PacketMetaHeader)
	if err != nil {
		this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
		return
	}

	ctx := metadata.NewOutgoingContext(context.Background(), md)

	var replyB []byte

	var header, trailer metadata.MD

	//add timeout
	rpcTimeout := esrpcgateway.DefaultCallTimeout
	rpcTimeoutMs, err := strconv.Atoi(pkt.RpcOption["rpc-timeout"])
	if err == nil {
		if rpcTimeoutMs < 1000 {
			rpcTimeoutMs = 1000
		}
		rpcTimeout = time.Duration(rpcTimeoutMs) * time.Millisecond
	}
	ctx, cancel := context.WithTimeout(ctx, rpcTimeout)
	defer cancel()

	donech := make(chan struct{})
	defer close(donech)

	go func() {
		tmr := time.NewTimer(rpcTimeout)
		defer tmr.Stop()
		select {
		case <-donech:
			return
		case <-tmr.C:
			cancel()
			return
		}
	}()

	replyB, err = grpcConn.InvokeForward(ctx, api, pkt.PacketBody,
		grpc.Header(&header),   // will retrieve header
		grpc.Trailer(&trailer), // will retrieve trailer
	)
	if nats2grpcDebug {
		glog.Info(zcodeutil.NowTimeStrInLocal(), "nats id:", natsID, "UnaryCall:", api, "err:", err)
	}
	if err != nil {
		this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
		return
	} else {
		subject := msg.Reply
		headerData, _ := esrpcgateway.MetaEncode(header)
		trailerData, _ := esrpcgateway.MetaEncode(trailer)

		resRpcPkt := esrpc.RpcPacketFromVTPool()
		resRpcPkt.RpcCode = esrpc.RpcCode_RPC_CODE_UNARY_OK
		resRpcPkt.PacketBody = replyB
		resRpcPkt.PacketMetaHeader = headerData
		resRpcPkt.PacketMetaTrailer = trailerData
		resRpcPkt.SeqNo = pkt.SeqNo
		defer resRpcPkt.ReturnToVTPool()

		data, err := resRpcPkt.MarshalVT()
		if err != nil {
			glog.Info("onUnary MarshalVT err:", err)
			this.publishErr(pkt, esrpc.RpcCode_RPC_CODE_UNARY_ERR, err.Error(), msg)
			return

		}
		err = this.natsConn.Publish(subject, data)
		if err != nil {
			glog.Info("onUnary publish err:", err)

		}
	}

}
func (this *TgatewayItem) rpcConnectErrCode(pkt *esrpc.RpcPacket) esrpc.RpcCode {
	switch pkt.RpcCode {
	case esrpc.RpcCode_RPC_CODE_UNARY:
		return esrpc.RpcCode_RPC_CODE_UNARY_ERR
	case esrpc.RpcCode_RPC_CODE_CLIENT_STREAM:
		return esrpc.RpcCode_RPC_CODE_CLIENT_STREAM_ERR
	case esrpc.RpcCode_RPC_CODE_SERVER_STREAM:
		return esrpc.RpcCode_RPC_CODE_SERVER_STREAM_ERR
	case esrpc.RpcCode_RPC_CODE_BIDI_STREAM:
		return esrpc.RpcCode_RPC_CODE_BIDI_STREAM_ERR
	}
	return esrpc.RpcCode_RPC_CODE_ERR
}
func (this *TgatewayItem) rpcConnectOkCode(pkt *esrpc.RpcPacket) esrpc.RpcCode {
	switch pkt.RpcCode {
	case esrpc.RpcCode_RPC_CODE_UNARY:
		return esrpc.RpcCode_RPC_CODE_UNARY_OK
	case esrpc.RpcCode_RPC_CODE_CLIENT_STREAM:
		return esrpc.RpcCode_RPC_CODE_CLIENT_STREAM_OK
	case esrpc.RpcCode_RPC_CODE_SERVER_STREAM:
		return esrpc.RpcCode_RPC_CODE_SERVER_STREAM_OK
	case esrpc.RpcCode_RPC_CODE_BIDI_STREAM:
		return esrpc.RpcCode_RPC_CODE_BIDI_STREAM_OK
	}
	return esrpc.RpcCode_RPC_CODE_UNSPECIFIED
}

func (this *TgatewayItem) onRpcStream(pkt *esrpc.RpcPacket, msg *nats.Msg) {
	if len(msg.Reply) == 0 {
		//no reply,no need to reply
		return
	}
	if pkt.RpcOption == nil {
		//no option,can not process
		this.publishErr(pkt, this.rpcConnectErrCode(pkt), "no rpc option", msg)
		return
	}
	sys := pkt.RpcOption["rpc-sys"]
	if this.sys != sys {
		this.publishErr(pkt, this.rpcConnectErrCode(pkt), "sys not match:["+sys+"]:["+this.sys+"]", msg)
		return
	}
	api := pkt.RpcOption["rpc-api"]
	if len(api) == 0 {
		this.publishErr(pkt, this.rpcConnectErrCode(pkt), "no rpc api rpcOption", msg)
		return
	}
	callid := pkt.RpcOption["rpc-callid"]
	if len(callid) == 0 {
		this.publishErr(pkt, this.rpcConnectErrCode(pkt), "no rpc callid rpcOption", msg)
		return
	}
	natsID := pkt.RpcOption["rpc-nats-clientid"]
	if len(natsID) == 0 {
		this.publishErr(pkt, this.rpcConnectErrCode(pkt), "no rpc nats clientid rpcOption", msg)
		return
	}
	grpcConn, err := this.grpcServiceDiscover(sys, api)
	if err != nil {
		this.publishErr(pkt, this.rpcConnectErrCode(pkt), err.Error(), msg)
		return
	}
	md, err := esrpcgateway.MetaDecode(pkt.PacketMetaHeader)
	if err != nil {
		this.publishErr(pkt, this.rpcConnectErrCode(pkt), err.Error(), msg)
		return
	}

	rpcStream := TgrpcStream{
		rpcType:         pkt.RpcCode,
		api:             api,
		sys:             sys,
		callid:          callid,
		natsclientid:    natsID,
		natsConn:        this.natsConn,
		apiSubscription: atomic.Pointer[nats.Subscription]{},
		grpcStream:      nil,
		GrpcConn:        grpcConn,
		CancelFn:        nil,
		HasCancel:       atomic.Bool{},
		destroyed:       atomic.Bool{},
		lastGrpcTime:    time.Now(),
		lastNatsTime:    time.Now(),
	}

	err = rpcStream.setup(md)
	if err != nil {
		this.publishErr(pkt, this.rpcConnectErrCode(pkt), err.Error(), msg)
		rpcStream.destroy()
		return
	}

	this.publishConnectStreamOK(pkt, this.rpcConnectOkCode(pkt), msg)

	rpcStream.run()
}

func RemoveGateway(sys string) (old *TgatewayItem, hasOld bool) {
	old, hasOld = nats2grpcGateways.Load(sys)
	if hasOld {
		nats2grpcGateways.Delete(sys)
		_ = old.apiSubscription.Unsubscribe()
	}
	return old, hasOld

}
func GetGateway(sys string) *TgatewayItem {
	value, _ := nats2grpcGateways.Load(sys)
	return value
}
func SetupGrpcGateWay(sys string, natsConn *nats.Conn, grpcServiceDiscover esrpcgateway.FnGrpcServiceDiscover) (gateway *TgatewayItem, err error) {
	old, hasOld := nats2grpcGateways.Load(sys)
	if hasOld {
		_ = old.apiSubscription.Unsubscribe()
	}
	gateway = &TgatewayItem{
		sys:                 sys,
		natsConn:            natsConn,
		grpcServiceDiscover: grpcServiceDiscover,
	}
	nats2grpcGateways.Store(sys, gateway)
	err = gateway.setupApiSubscription()
	if err != nil {
		RemoveGateway(sys)
		return nil, err
	}
	return gateway, nil
}
